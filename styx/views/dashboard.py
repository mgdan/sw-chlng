from django.shortcuts import render, redirect


def dashboard(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login/')

    context = {}
    
    return render(request, "dashboard.html", context)
